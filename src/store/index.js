import Vue from 'vue'
import Vuex from 'vuex'
import project_config from '@/assets/project_config.json'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        project_config, //项目相关配置
        user_data:null, //储存登陆用户的信息
        upload_file:{}, //添加的上传文件对象（包括其他的属性数据）
        selection_files:[], //已选择的文件（提供下载等其他操作）
        search_data:"", //搜索的字符串
        loading:{ //全局加载状态
            state:false, //加载状态
            msg:'正在加载•••', //加载的提示
        },
        upload_dialog:false, //控制上传文件对话框的显示
        delete_dialog:false, //控制删除文件对话框的显示
        share_dialog:false, //控制分享文件对话框的显示
        unshare_dialog:false, //取消分享文件对话框的显示
        update_data:false, //全局是否更新数据列表，(当文件删除后通知列表刷新)
    },
    mutations: {
        set_user_data(state,user_data){ //写入用户数据，同时写入本地储存
            state.user_data = user_data;
            sessionStorage.setItem('user_data',JSON.stringify(user_data));
        },
        remove_user_data(state){ //清空用户数据，同时清空相应的本地储存
            state.user_data = null;
            sessionStorage.removeItem('user_data');
        },
        add_upload_file(state,file){
            state.upload_file = file;
        },
        set_selection_files(state,selection_files){ //写入已选择的文件
            state.selection_files = selection_files;
        },
        remove_selection_files(state){ //清空已选择的文件
            state.selection_files = [];
        },
        set_search_data(state,search_data){ //写入搜索的字段
            state.search_data = search_data;
        },
        remove_search_data(state){ //清空搜索的字段
            state.search_data = '';
        },
        set_loading(state,loading){ //写入loading对象
            for(let a in state.loading){
                if(loading[a] !== undefined){
                    state.loading[a] = loading[a];
                }
            }
        },
        set_upload_dialog(state,show){ //设置上传文件对话框的显示
            state.upload_dialog = show;
        },
        set_delete_dialog(state,show){ //设置删除文件对话框的显示
            state.delete_dialog = show;
        },
        set_share_dialog(state,show){ //设置分享文件对话框的显示
            state.share_dialog = show;
        },
        set_unshare_dialog(state,show){ //写入需要取消分享的文件
            state.unshare_dialog = show;
        },
        set_update_data(state,data){ //设置全局更新的状态
            state.update_data = data;
        },
    },
    actions: {
    },
    modules: {
    }
});
