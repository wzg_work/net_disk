import Vue from 'vue'
import App from './App.vue'
//引入路由
import router from './router'
//引入状态管理
import store from './store'
//引入elementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
//引入公共的CSS
import '@/assets/public_style.scss'
//引入axios
import axios from "./common/http";
Vue.prototype.$axios = axios;

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
