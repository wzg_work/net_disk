function get_fileicon_by_type(_type){ //根据文件类型获得问价图标类名
    let _icon;
    switch(_type){
        case 'folder': //文件夹
            _icon = "icon-folder-m";
            break;
        case 'zip': //压缩包
            _icon = "icon-zip-m";
            break;
        case 'doc': //文档
            _icon = "icon-docx-m";
            break;
        case 'docx': //文档
            _icon = "icon-docx-m";
            break;
        case 'xls': //表格
            _icon = "icon-xls-m";
            break;
        case 'xlsx': //表格
            _icon = "icon-xls-m";
            break;
        case 'ppt': //ppt
            _icon = "icon-ppt-m";
            break;
        case 'pdf': //pdf
            _icon = "icon-pdf-m";
            break;
        case 'img': //img
            _icon = "icon-img-m";
            break;
        case 'txt': //txt
            _icon = "icon-xls-m";
            break;
        case 'md': //md 
            _icon = "icon-md-m";
            break;
        default: //未知文件
            _icon = "icon-nor-m";
            break;
    }
    return _icon;
}

export {
    get_fileicon_by_type,
} 