import Vue from 'vue'
import VueRouter from 'vue-router'
import Disk from '@/views/disk.vue'
import Login from '@/views/login.vue'
import Share from '@/views/share.vue'
import All from '@/views/disk/right/all.vue'
import Audio from '@/views/disk/right/audio.vue'
import Doc from '@/views/disk/right/doc.vue'
import Photo from '@/views/disk/right/photo.vue'
import Recent from '@/views/disk/right/recent.vue'
import Video from '@/views/disk/right/video.vue'
import Folder from '@/views/disk/right/folder.vue'

Vue.use(VueRouter)

const routes = [
    {
        path:'/login', //登陆
        name:'login',
        component:Login,
    },
    {
        path:'/share', //文件分享
        name:'share',
        component:Share,
    },
    {
        path:'/disk',
        name:'disk',
        component:Disk,
        children:[
            {
                path:'', //全部
                name:'all',
                component:All,
            },
            {
                path:'audio', //音乐
                name:'audio',
                component:Audio,
            },
            {
                path:'doc', //文档
                name:'doc',
                component:Doc,
            },
            {
                path:'photo', //图片
                name:'photo',
                component:Photo,
            },
            {
                path:'recent', //最近
                name:'recent',
                component:Recent,
            },
            {
                path:'video', //视频
                name:'video',
                component:Video,
            },
            {
                path:'folder', //文件夹
                name:'folder',
                component:Folder,
            },
        ],
    },
];

const router = new VueRouter({
    //mode: 'history',
    routes,
});

function get_admin_state(){  //判断用户登陆状态
   return window.sessionStorage.getItem('user_data')?true:false;
}

router.beforeEach((to, from, next) => {
    if(to.path!=="/login"){
        if(!get_admin_state()){
            next('/login');
        }else if(to.path=="/"){
            next('/disk');
        }else{
            next();
        }
    }else{
        next();
    }
});

export default router;
