module.exports = {
    // 选项...
    publicPath:'./',
    outputDir:'dist',
    assetsDir:'assets',
    devServer: {
        host: 'localhost',
        port: 8080,
        proxy: { 
            '/api': {
                target: `http://30.1.2.54:8080`,
                changeOrigin: true,
                pathRewrite: {
                    ['^/api']: '',
                }
            }
        },
        //disableHostCheck: true,
    },
}